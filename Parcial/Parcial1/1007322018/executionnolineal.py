import noLineal as nl
import numpy as np

if __name__ == '__main__':

    def f(u,y,t):
        g=-10
        l=1
        return (-g/l)*np.sin(u)

    pendulo=nl.PenduloNoLineal(1,0,f)


    print(pendulo.DesplazamientoAngular())
