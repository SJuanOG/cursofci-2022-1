from POO import *
import numpy as np

if __name__ == '__main__':
    
    #Se instancia un objeto simulando la gravedad en la tierra
    Tierra = PosicionYGrafica(vel_in=2,angulo=45,gravedad=9.8,ac_viento=1.2,altura=2)
    Tierra.Grafica()
