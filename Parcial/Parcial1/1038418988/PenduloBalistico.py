import numpy as np
class PenduloBalistico:
    
    def __init__(self, M, m, l): #Inicializamos la clase.
        self.M=M #Definimos atributos.
        self.m=m
        self.l=l
    
    def VelocidadBala(self,theta): #Definimos los metodos.
        self.theta=theta #Valido para desviaciones de 0 a 180 grados.
        t=self.theta*(np.pi)/180 
        if 0<=self.theta<=90: #Se evaluan las opciones para las desviaciones angulares.
            return (1+(self.M/self.m))*np.sqrt(2*9.8*self.l*(1-np.cos(t)))
        else:
            ("Programa valido unicamente para 0 a 90 grados.")
    
    def DesviacionAngulo(self,u): #Valido para velocidades que se den de 0 a 90 grados de desviacion.
        self.u=u
        u1=(1+(self.M/self.m))*np.sqrt(2*9.8*self.l)
        if 0<=self.u<=u1: #Se evaluan las opciones para la velocidad.
            x=1-(0.5*((self.m*self.u)**2)/(9.8*self.l*((self.m+self.M)**2)))
            return (np.arccos(x))*(180/np.pi)
        else:
            ("Programa velocidades inferiores.")
        

class GiroCompleto(PenduloBalistico): #Herencia.
    def __init__(self, M, m, l): #Inicializamos la clase.
        self.M=M #Definimos atributos.
        self.m=m
        self.l=l
    def VelocidadMinima(self):
        return (1+(self.M/self.m))*np.sqrt(5*9.8*self.l)
