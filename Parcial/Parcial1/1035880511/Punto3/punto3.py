import numpy as np
import matplotlib.pyplot as plt

# Creación de la clase
class Pendulonolineal:
    # Inicialización. Se definen los atributos de: h, u0, v0, ti,tf
    def __init__(self,h,u0,v0,ti,tf): 
        self.h = h 
        self.u0 = u0 
        self.v0 = v0
        self.ti = ti
        self.tf = tf
        

    # Se crea en método "funct" un método para conseguir dos ecuaciones diferenciales de primer orden.
    def funct(self,t,y): # y corresponde a un arreglo que contiene las posiciones y las velocidades
        g = 9.8
        l = 1
        k = 0
        self.t = t
        f1 = y[1]
        f2 = -g/l * np.sin(y[0]) - k * y[1]
        a = np.array([f1,f2])

        return a 
        
    # Implementación de RK4. El método tiene como parámetro la variable con coordenadas y velocidades
    def RungeKutta(self,CVvect):
        self.t = self.ti
        
        self.k1 = self.h * self.funct(self.t,CVvect )
        self.k2 = self.h * self.funct(self.t + 0.5 * self.h , CVvect + 0.5 * self.k1)
        self.k3 = self.h * self.funct(self.t + 0.5 * self.h , CVvect + 0.5 * self.k2)
        self.k4 = self.h * self.funct(self.t + self.h , CVvect + self.k3)
        self.a0 = CVvect + 1/6 * ( self.k1 + 2 * self.k2 + 2 * self.k3 + self.k4 )
    
        return self.a0 

    
    #  Se crea el método "DesplazamientoAngular" para calcular las posiciones y velocidaades
    def DesplazamientoAngular(self):
        
        # Se inicializa el contador y un parámetro k para el while
        k = self.ti
        i = 0
        n = round(( self.tf - self.ti) / self.h ) # Longuitud del intervalo
        Y = np.zeros( (2, n) ) # Se define la dimensión del arreglo de posiciones y velocidades Y
        CVvect= np.array( [self.u0, self.v0] )
    
    # Ciclo for para el cálculo de las coordenadas
        for j in np.arange(self.ti, self.tf, self.h):
            
            Y[:,i] = CVvect  
            CVvect = self.RungeKutta(CVvect) # Va creando vector posiciones y velocidades a partir del método definido "RungeKutta"
            k = k + self.h
            i = i + 1
        
        usol = Y[0] # Arreglo final de posiciones
        vsol = Y[1] # Arreglo final de velocidades

        return usol

            





            
             


    












