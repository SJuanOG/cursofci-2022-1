#include <iostream>
#include <cstdlib>  // Libreria para generar numero aleatorio y semilla para cambiar su valor
#include <ctime>    // Libreria para recuperar la hora del sistema

using namespace std;

int User_number;
bool quit; 

int GenerateRandomNumber(){
    int random_number = rand() % 1000 + 1;
    return random_number;
}

void AdivinarNumero(){
    srand(time(NULL));  // Generando semilla dependiendo del tiempo actual
    // para que en cada iteración el numero aleatorio cambie (sin este, se vuelve a repetir el numero)
    cout << "Bienvenido al juego de la adivinanza!! Tienes que adivinar un numero del 1 al 1000" << endl;
    int N = GenerateRandomNumber();
    cout << "Ingrese un número del 1 al 1000" << endl;
    cin >> User_number;
    quit = false;

    while (not quit){
        
        if (User_number == N){
            cout << "Has adivinado el número "<< endl;
            quit = true;
        }

        else if (User_number < N){
            cout << "El número a adivinar es mayor" << endl;
            cout << "Ingrese otro número del 1 al 1000" << endl;
            cin >> User_number;
        }

        else{
            cout << "El número a adivinar es menor" << endl;
            cout << "Ingrese otro número del 1 al 1000" << endl;
            cin >> User_number;
        }
    }
}

int main(){
    char answer;
    bool Game = true;
    while (Game){
        AdivinarNumero();
        cout << "Quieres volves a jugar? y/n" << endl;
        cin >> answer;
        if (answer == 'y' | answer == 'Y'){
            Game = true;
        }
        else if(answer == 'n' | answer == 'N'){
            Game = false;
        }
        else{
            cout << "Tecla incorrecta (y/n). Finalizando juego" << endl;
            Game = false;
            
        }
    }
    return 0;
}
