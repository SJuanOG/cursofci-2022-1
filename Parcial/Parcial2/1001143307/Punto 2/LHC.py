import random

class LHC:
    def __init__(self, Radio, radio_particle1, radio_particle2):
        self.R = Radio
        self.r1 = radio_particle1
        self.r2 = radio_particle2

    def particle1(self):
        x1 = random.uniform(-(self.R - self.r1), self.R - self.r1)
        y1 = random.uniform(-(self.R - self.r1), self.R - self.r1)
        return x1, y1

    def particle2(self):
        x2 = random.uniform(-(self.R - self.r2), self.R - self.r2)
        y2 = random.uniform(-(self.R - self.r2), self.R - self.r2)
        return x2, y2

    def probablities(self, N):
        P = []
        successes = []
        for _ in range(N):
            x1, y1 = self.particle1()
            x2, y2 = self.particle2()
            r_choque = ((x2 - x1) ** 2 + (y2 - y1) ** 2) ** 0.5
            if r_choque <= self.r1 + self.r2:
                successes.append(1)
            else:
                successes.append(0)
            
            succ = sum(successes) / len(successes)
            P.append(succ)

        return P
