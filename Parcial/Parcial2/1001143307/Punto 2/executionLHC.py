from LHC import LHC
import numpy as np
import matplotlib.pyplot as plt

def main():
    # Radio de las particulas
    r = 0.2
    # Radio del tubo LHC
    R = 100
    exp1 = LHC(R, r, r)
    Num_exp = 100000
    Probabilitys = exp1.probablities(Num_exp)
    x = np.arange(1, Num_exp + 1, 1)
    plt.title(f'Particulas con radio {r} y radio del LHC: {R}')
    plt.plot(x, Probabilitys, '.--', c='orange')
    plt.xlabel('Número de experimento')
    plt.ylabel('Probabilidad')
    plt.grid()
    plt.show()


if __name__ == '__main__':
    main()

# Suponiendo que las particulas que entran en el LHC (circular), 
# permanecen con el mismo radio de trayectoria respecto al centro
# del LHC, entonces el problema se basaría en hallar el choque en la sección transversal
# donde las particulas se intersectarán, es decir, el problema sería analizarlo
# como lineal, ya que si una particula se dispara en un principio desde el centro
# del tubo del LHC entonces este va a seguir esa misma posición cuando empiece
# a moverse, por tanto sería analizar si las dos particulas lanzadas contrariamente
# se chocarán en esa sección transversal, como se hizo en clase (lineal).
# Es decir que no hay diferencia entre el experimento lineal y circual
# suponiendo que las particulas siguen la trayectoria con el mismo radio
# respecto al centro de todo el LHC.
