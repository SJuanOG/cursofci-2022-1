#include <iostream> //importar librerìas. (opcional)

using namespace std; //Importar todo lo que tiene la librerìa estàndar por defecto. (opcional)


int main() 
{ 
    int n, num, i, continuar;

    continuar=1; //Se da la opciòn de continuar.
    while (continuar==1) //Se da la opciòn de volver a jugar.
    {
        n = rand() % 1000; //Se define el nùmero que se va a adivinar.
        i=1; //Se inicializa la iteraciòn.
        cout << "Intento nùmero:" << i << ". "; //Se itera.
        cout << "Ingrese un nùmero de 0 a 1000:"; //Se ingresa el nùmero para adivinar.
        cin >> num;
        while (num!=n) //Mientras sea diferente, se realizarà el mismo procedimiento.
        {
            if (num<n)
            {
                cout << "Su nùmero es menor."; //Si el nùmero ingresado es menor.
            }
            else
            {
                cout << "Su nùmero es mayor."; //Si el nùmero ingresado es mayor.
            }
            cout << "Intento nùmero:" << (i+=1)<< ". "; //Se itera nuevamente.
            cout << "Ingrese otro nùmero de 0 a 1000:"; //Se intenra de nuevo.
            cin >> num;
        }
        if (num==n)
        {
            cout << "Ese es el nùmero."; //Si el nùmero es correcto, acaba.
            cout << "¿Desea jugar nuevamente?"<< " "<< "1. Sì." << " "<< "2. No."; //Pregunta si desea seguir jugando.
            cin >> continuar; //Una vez terminado, se puede volver a jugar.
        }
    }
    getchar();
    return 0;
}
