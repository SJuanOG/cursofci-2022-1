#include <iostream>
using namespace std;
#include <ctime>
class AdivinarNumero
{ public:
	 int num;// número aleatorio
	 int val ;// número ingresado por el usuario
	 int vol ;// opcion de volver a jugar
	 float adivina();// funcion del juego
	 float intento();// funcion para volver a jugar
} A;
float AdivinarNumero::adivina()
{
	srand((unsigned int)time(NULL)); //semilla
	num = rand() % 1000 + 1; // número random
	val = 0;
	while (1)
	{
		cout << "número que crees que escogio la maquina:";
		cin >> val;
		if (val < num)
		{
			cout << "número es menor al escogido por la maquina" << endl;
		}
		else if (val > num)
		{
			cout << "El número es mayor al escogido" << endl;
		}
		else
		{
			cout << "¡Haz ganado!" << endl;
		}
	}
	return 0;
}	
float AdivinarNumero::intento(){
	vol =0;

	while(1){
	cout << "Si quieres volver a jugar escribe un 1 uno y dale enter de lo contrario cualquier tecla y enter" << endl;
	cin >> vol;
	if (vol == 1)
	{
		A.adivina();
    }
	else{
		cout << "Gracias por jugar" << endl;
		break;
	}
	}

    return 0;

}

int main(){
    A.adivina();
	A.intento();
    system("pause");
    return 0;
}