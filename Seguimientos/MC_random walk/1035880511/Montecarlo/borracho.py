import numpy as np
import matplotlib.pyplot as plt



# Graficar la trayectoria del borracho 
# Calcular la probabilidad de que llegue a las 2 cuadras
# 50 es el número de pasos

posx = [0]
posy = [0] 

N = 50


while N > 0 :
    #for i in range(0,10):
        nr = np.random.randint(0,100)

        if nr <= 25:
            posy.append(posy[0]+1)
            posx.append(0)
        elif 25 < nr <= 50:
            posy.append(posy[0]-1)
            posx.append(0)
        elif 50 < nr <= 75:
            posx.append(posx[0]+1)
            posy.append(0)
        elif 75 < nr <= 100:
            posx.append(posx[0]-1)
            posy.append(0)

        
        N = N-1
# Se genera el acumulado y se grafica la posición final
newposx = np.cumsum(posx)
newposy = np.cumsum(posy)
plt.plot(newposx,newposy,'-o')
plt.title('Trayectoria')
plt.plot(newposx[-1],newposy[-1],'-o', color = 'red')
plt.plot(newposx[0],newposy[0],'o',color ='green')
plt.xlabel('Posición en x')
plt.ylabel('Posición en y')
plt.show()



