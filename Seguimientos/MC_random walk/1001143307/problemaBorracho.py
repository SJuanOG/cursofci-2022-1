import random
import matplotlib.pyplot as plt
import numpy as np

# Funcion para retornar la pos final del
# borracho luego de N iteraciones (pasos)
def MovimientoBorracho(N):
    '''N: Número de pasos que da el borracho'''
    # Pos inicial del borracho
    x, y = 0, 0
    X = Y = []

    for _ in range(N):
        n = random.randint(1, 100)
        if n <= 25:
            y += 1
        if 25 < n <= 50:
            y -= 1
        if 50 < n <= 75:
            x += 1
        if n > 75:
            x -= 1

        X.append(x)
        Y.append(y)

    return x, y

# Funcion para las probabilidades 
def Prob(M):
    '''M: Número de experimentos'''
    # Lista para guardar las probabilidades 
    P = []
    # Guardar el exito de que el borracho este a 2 cuadras exactamente
    P_s = []

    for _ in range(M):
        x, y = MovimientoBorracho(N=10)  # N=10 pasos que da el borracho
        if abs(x) + abs(y) == 2:
            # Exito
            P_s.append(1)
        else:
            # No-exito
            P_s.append(0)
        # Probailidad de exito    
        proba_exito = sum(P_s) / len(P_s)
        P.append(proba_exito)

    return P

def main():
    Num_expe = 1000
    Probabilidades = Prob(Num_expe)
    x = np.arange(1, Num_expe + 1, 1)
    plt.plot(x, Probabilidades, '.--', c='orange')
    plt.title('Probabilidad de que el borracho esté a 2 cuadras')
    plt.xlabel('Número de experimento')
    plt.ylabel('Probabilidad')
    plt.grid()
    plt.show()

if __name__ == '__main__':
    main()
