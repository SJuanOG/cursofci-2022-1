
import numpy as np

class metodosNumericos:
    
    '''
    Clase que contendrá varios métodos númericos para la solución de EDO
    El primero de ellos: Método de Euler, para EDO de primer orden y lineales
    Los parametros iniciales de la clase son:
    params: fp--> ED0   {dy/dx = fp(x,y)}
            (x0,y0): conidicón inicial de la EDO
            h: Intervalo de muestreo, entre más pequeño, más exacto

    '''

    def __init__(self,fp,x0,y0,h):
        self.fp = fp
        self.x0 = x0
        self.y0 = y0
        self.h = h
    
    '''
    Método de Euler.
    Params: x --> valor para calcular la soculión de la EDO en ese punto
    return: y(x) --> la solución aproximada de la función y en el valor x
    '''
    def metodoEuler(self,x):
        
        self.n = (x - self.x0)/self.h
        x0 = self.x0
        y0 = self.y0
        X = [x0]
        Y = [y0]
        for i in np.arange(self.x0,x,self.h):
            y0 += self.h*self.fp(x0,y0)
            x0 += self.h
            X.append(x0)
            Y.append(y0)
 
        return Y[-1]  #Ultimo elemento donde está la solución aproximada para x

    '''
    Método Runge-kutta.
    Params.
        x: valor para retornar la solucion más aproximada evaluada en este punto
    '''

    def rk4(self,x):

        self.n = (x-self.x0)/self.h
        xn = self.x0
        yn = self.y0

        for j in np.arange(self.x0,x,self.h):
            
            k1 = self.h*self.fp(xn,yn)
            k2 = self.h*self.fp(xn + 1/2*self.h,yn + 1/2*k1)
            k3 = self.h*self.fp(xn+1/2*self.h,yn+1/2*k2)
            k4 = self.h*self.fp(xn+self.h,yn+k3)

            xn += self.h
            yn += 1/6*(k1+2*k2+2*k3+k4)
        
        return yn  #Ultimo elemento acumulado donde estará el y(x)
            


