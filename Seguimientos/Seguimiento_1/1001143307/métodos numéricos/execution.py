
from metodosNumericos import metodosNumericos
import numpy as np

if __name__ == '__main__':
    
    '''
    Solución analitica de la EDO dy/dx = 2xy con conidcion inicial y(1) = 1
    '''
    def y(x):
        f = 1/np.e*np.e**x**2
        return f

    x0 = 1
    y0 = 1
    h = 0.001

    EDO1 = metodosNumericos(lambda x,y:2*x*y,x0,y0,h)
    print(f'Con método de Euler: {EDO1.metodoEuler(1.5)}')
    print(f'Con método de Runge-kutta: {EDO1.rk4(1.5)}')

    print(f'Solución analitica: y(1.5)= {y(1.5)}')