import numpy as np
import matplotlib.pyplot as plt 
from mpl_toolkits.mplot3d import axes3d
import trayectoria as tr



t1=np.linspace(0,20,500)
if __name__=='__main__':
    #Extraigamos  arreglos con los valores de cada variable en el tiempo.
    coordenada1=tr.trayectorias(np.pi/6,np.pi/3,600e-6,9.11e-31,18.6,t1)
    xx=coordenada1.posx()
    coordenada2=tr.trayectorias(np.pi/6,np.pi/3,600e-6,9.11e-31,18.6,t1)
    yy=coordenada1.posy()
    coordenada3=tr.trayectorias(np.pi/6,np.pi/3,600e-6,9.11e-31,18.6,t1)
    zz=coordenada3.posz()
    zzz=np.array([zz])
    
    #Ahora grafiquemos
    
    fig = plt.figure(figsize=(9,5))
    ax = plt.axes(projection='3d')
    ax.plot3D(xx, yy, zz, 'red')
    ax.set_xlabel("Eje x")

    ax.set_ylabel("Eje y")

    ax.set_zlabel("Eje z")
    ax.set_title('Trayectoria de la partícula cargada, en este caso un protón.')
    plt.legend()
    plt.show()








