from matplotlib import projections
import trayect as trayect
import numpy as np
import matplotlib.pyplot as plt

# Start
if __name__=='__main__':
    print('Esta es una inicialización')
    # Creación del arreglo de tiempo
    t = np.arange(0,2,0.01)
    # Se define el objeto "electrón"
    electron=trayect.Trayect(600e-6,18.6*1.60e-19,np.pi/6,9.11e-31,-1.602e-19,t)
    
    posex=electron.x()
    posey=electron.y()
    posez=electron.z()
    # Se define el objeto "positrón"
    positron=trayect.Trayect(600e-6,18.6*1.60e-19,np.pi/6,9.11e-31,1.602e-19,t)
    pospx=positron.x()
    pospy=positron.y()
    pospz=positron.z()
    
    # Se genera una gráfica 3D de la trayectoria
    fig = plt.figure()
    ax = plt.axes(projection='3d')
    ax.plot3D(posex, posey, posez,color='purple',label='Electron')
    ax.plot3D(pospx,pospy,pospz,color='orange',label='Positron')
    ax.set_title('Particle Trajectory')
    ax.legend()
    plt.show()

    # Se genera una gráfica de las coordenadas
    helip=electron.heli()[0] # Retur first value
    helin=electron.heli()[1] # Retur second value
    phelip=positron.heli()[0] # Retur first value
    phelin=positron.heli()[1] # Retur second value
    fig= plt.figure()
    ax = plt.axes(projection='3d')
    ax.plot3D(helip, posey, posez,color='purple',label='Electron')
    ax.plot3D(helin, posey, posez,color='purple')
    ax.plot3D(phelip, posey, posez,color='orange',label='Positron')
    ax.plot3D(phelin, posey, posez,color='orange')
    ax.set_title('Coordinate space for electron and positron')
    plt.legend()
    plt.show()
    

 



    