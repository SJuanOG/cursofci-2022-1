import numpy as np
from sympy import *

class metodos_numericos:
    
    #Constructor
    def __init__(self, y0, x0, paso, funcion, evaluacion_x):
        self.y0 = y0
        self.ex = evaluacion_x
        self.x0 = x0
        self.h = paso
        self.func = funcion
    
    #Metodo euler
    def euler(self):
        self.n = round((self.ex - self.x0)/self.h)   #Definición numero puntos
        x = np.zeros(self.n+1)           #Inicialización x
        x[0] = self.x0                 #Condición inicial x
        y = np.zeros(self.n+1)           #Inicialización y
        y[0] = self.y0                 #Condición inicial y
        #Implementación euler
        for i in range(self.n):           
            x[i+1] = self.h+x[i]            
            y[i+1] = y[i] + self.h*self.func(x[i],y[i])
        return x,y
    
    #Evaluar la función con método euler
    def mensaje_euler(self):
        x,y =  self.euler()
        return f'La función evaluada mediante el método de euler en el punto más cercano a ({self.ex}) es igual a ({y[-1]})' 
    
    #Método de Runge-Kutta4
    def runge_kutta4(self):
        self.n = round((self.ex - self.x0)/self.h)   #Definición numero puntos
        x = np.zeros(self.n+1)                 #Inicialización x
        x[0] = self.x0                       #Condición inicial x
        y = np.zeros(self.n+1)                 #Inicialización y
        y[0] = self.y0                       #Condición inicial y
        for i in range(self.n):
            x[i+1] = self.h+x[i]  
            k1 = self.h*self.func(x[i],y[i])
            k2 = self.h*self.func(x[i] + self.h/2, y[i] + k1/2)
            k3 = self.h*self.func(x[i] + self.h/2, y[i] + k2/2)
            k4 = self.h*self.func(x[i]+self.h,y[i]+k3)
            y[i+1] = y[i] + ( k1 + 2*(k2 + k3) + k4)/6
        return x,y
    
    #Evaluar la función con método euler
    def mensaje_rk4(self):
        x,y =  self.runge_kutta4()
        return f'La función evaluada mediante el método de Runge-Kutta-4 en el punto más cercano a ({self.ex}) es igual a ({y[-1]})' 
        
    #Método análitico
    def analitico(self):
        x = symbols('x')                                                    #Definición de simbolo
        y = Function('y')                                                   #Definición de función para integración
        gfg = sympify(str(self.func(x,y(x))))                               #Transformación de función en expresión simbolica
        expr=dsolve(y(x).diff(x) - gfg, ics={y(self.x0): self.y0})          #Integración análitica
        return f'la solución análitica se puede observar en el segúndo término de la expresión: {expr.evalf(subs = {x: self.ex})}'
        
