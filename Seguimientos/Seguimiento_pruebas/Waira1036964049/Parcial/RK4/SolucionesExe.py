import soluciones as eu 



if __name__=='__main__':

#Defina,os las funciones a resolver.
    def f1(x, y):
        sol = 2.*y+x
        return(sol)
    def f2(x,y):
        sol2=2*x*y
        return(sol2)

    print("Vamos a resolver el problema mediante el metodo de Euler,")
    s1= eu.euler(f1,0,1.5,15,2)
    print('este es:', s1.p_i())

    print("Vamos a resolver el problema mediante el metodo de Runge Kutta 4,")
    s11= eu.rk4(f2,1.5,1,5,1)
    print('este es:',s11.p_ii())

